package com.cerotid.bank.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cerotid.bank.Customer;

@Controller
public class BankController {

	// mapped to hostname:port#/bank/ and request to bank will be handled by
	@RequestMapping("/bank") // ModelAndView()
	public ModelAndView getCustomer(Customer customer) {
		System.out.println("Request is accepted"); // do not use sysout

		ModelAndView modelView = new ModelAndView();
		modelView.addObject("object", customer);
		modelView.setViewName("bank");

		return modelView;
	}

	@RequestMapping("/addCustomer")
	public ModelAndView addCustomer(Customer customer) {
		System.out.println("I am preparing to add customer");
		System.out.println("name = " + customer.getFirstName() + " " + customer.getLastName());

		// Customer add to data base logic inject here

		ModelAndView modelView = new ModelAndView();
		modelView.addObject("object", customer);
		modelView.setViewName("bank");

		return modelView;
	}

	@RequestMapping("/")
	public ModelAndView home() {
		System.out.println("I am preparing to go to Add Customer page");

		ModelAndView modelView = new ModelAndView();
		modelView.addObject(new Customer());
		modelView.setViewName("bank");

		return modelView;
	}

	@RequestMapping("/navigateToAddCustomer")
	public String navigateToAddCustomer() {
		return "addCustomer";
	}
}
